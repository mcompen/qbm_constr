#=
plotting:
- Julia version: 0.6.2
- Author: Manu Compen
- Date: 2018-06-11
=#

function plot_weights(Weights, Name)
    rect(w, h, x, y) = Shape(x + [0,w,w,0], y + [0,0,h,h])
    w, h, y = 1, 1, 0

    ax = plot(legend=false, color="gray", yaxis=false, xticks=0:1:n, grid="off", size=(600,150), title="|w_x_max| = $(maximum(abs, Weights.w_x))")
    for i=1:n
        shape = rect(w, h, i, y)
        ax = plot!(shape, color="gray", opacity=(Weights.w_x[i] - minimum(Weights.w_x)) / (maximum(Weights.w_x) - minimum(Weights.w_x)))
    end
    savefig(string(Name,n,"wx.png"))

    ax = plot(legend=false, color="gray", yaxis=false, xticks=0:1:n, grid="off", size=(600,150), title="|w_y_max| = $(maximum(abs, Weights.w_y))")
    for i=1:n
        shape = rect(w, h, i, y)
        ax = plot!(shape, color="gray", opacity=(Weights.w_y[i] - minimum(Weights.w_y)) / (maximum(Weights.w_y) - minimum(Weights.w_y)))
    end
    savefig(string(Name,n,"wy.png"))

    ax = plot(legend=false, color="gray", yaxis=false, xticks=0:1:n, grid="off", size=(600,150), title="|w_z_max| = $(maximum(abs, Weights.w_z))")
    for i=1:n
        shape = rect(w, h, i, y)
        ax = plot!(shape, color="gray", opacity=(Weights.w_z[i] - minimum(Weights.w_z)) / (maximum(Weights.w_z) - minimum(Weights.w_z)))
    end
    savefig(string(Name,n,"wz.png"))

    plotxx = heatmap(flipdim(Weights.w_xx, 1), color=:gray); # Flipdim since heatmap transposes data in Plots (https://github.com/JuliaPlots/Plots.jl/issues/196). Random addition since heatmap returns an empty plot otherwise (?).
    savefig(string(Name,n,"wxx.png"))

    plotyy = heatmap(flipdim(Weights.w_yy, 1), color=:gray);
    savefig(string(Name,n,"wyy.png"))

    plotzz = heatmap(flipdim(Weights.w_zz, 1), color=:gray);
    savefig(string(Name,n,"wzz.png"))
end

function plot_statistics(ModelStat, Name)
    rect(w, h, x, y) = Shape(x + [0,w,w,0], y + [0,0,h,h])
    w, h, y = 1, 1, 0

    ax = plot(legend=false, color="gray", yaxis=false, xticks=0:1:n, grid="off", size=(600,150), title="|x_max| = $(maximum(abs, ModelStat.σ_x))")
    for i=1:n
        shape = rect(w, h, i, y)
        ax = plot!(shape, color="gray", opacity=(real(ModelStat.σ_x[i]) - minimum(real(ModelStat.σ_x))) / (maximum(real(ModelStat.σ_x)) - minimum(real(ModelStat.σ_x))))
    end
    savefig(string(Name,n,"x.png"))

    ax = plot(legend=false, color="gray", yaxis=false, xticks=0:1:n, grid="off", size=(600,150), title="|y_max| = $(maximum(abs, ModelStat.σ_y))")
    for i=1:n
        shape = rect(w, h, i, y)
        ax = plot!(shape, color="gray", opacity=(real(ModelStat.σ_y[i]) - minimum(real(ModelStat.σ_y))) / (maximum(real(ModelStat.σ_y)) - minimum(real(ModelStat.σ_y))))
    end
    savefig(string(Name,n,"y.png"))

    ax = plot(legend=false, color="gray", yaxis=false, xticks=0:1:n, grid="off", size=(600,150), title="|z_max| = $(maximum(abs, ModelStat.σ_z))")
    for i=1:n
        shape = rect(w, h, i, y)
        ax = plot!(shape, color="gray", opacity=(real(ModelStat.σ_z[i]) - minimum(real(ModelStat.σ_z))) / (maximum(real(ModelStat.σ_z)) - minimum(real(ModelStat.σ_z))))
    end
    savefig(string(Name,n,"z.png"))

    plotxx = heatmap(flipdim(real(ModelStat.σ_xx), 1), color=:gray); # Flipdim since heatmap transposes data in Plots (https://github.com/JuliaPlots/Plots.jl/issues/196)
    savefig(string(Name,n,"xx.png"))

    plotyy = heatmap(flipdim(real(ModelStat.σ_yy), 1), color=:gray);
    savefig(string(Name,n,"yy.png"))

    plotzz = heatmap(flipdim(real(ModelStat.σ_zz), 1), color=:gray);
    savefig(string(Name,n,"zz.png"))
end

function plot_likelihood(llhood)
    ax = plot(-llhood, yscale= :log10, label="-likelihood", xlabel="iter", ylabel="L");
    savefig(string(n,"llhood.png"))
end

function plot_psi_err(psi_err)
    ax = plot(psi_err, label="psi error");
    savefig(string(n,"psi_err.png"))
end

function plot_psis(ψ_η, ψ_ρ)
    if ~isreal(ψ_ρ)
        println("ψ_ρ not real: plotting real part")
    elseif ~isreal(ψ_η)
        println("ψ_η not real: plotting real part")
    end

    ψ_η = real.(ψ_η) / norm(real.(ψ_η))
    ψ_ρ = real.(ψ_ρ) / norm(real.(ψ_ρ))

    if norm(ψ_η + ψ_ρ) < norm(ψ_η - ψ_ρ)
        ψ_ρ = -ψ_ρ
    end

    ax = plot(ψ_η, ψ_η, color="red", label="true/true", xlabel="true psi", ylabel="model psi");
    ax = scatter!(ψ_η, ψ_η, color="red", markersize=13, label="true psi");

    ax = scatter!(ψ_η, ψ_ρ, color="blue", markersize=10, label="model psi");
    savefig(string(n, "psis.png"))
end

