#=
network_r:
- Julia version: 0.6.2
- Author: Manu Compen
- Date: 2018-09-20
=#

const m = n * α                               # Number of hidden neurons
const dim = n * m + m + n                     # Total amount of free parameters

function init_params(ParamStruct)            # Initiates the parameters and passes them to the struct
    state = 2 * rand(0:1, n) - 1
    a = complex(1,0) * rand(Normal(0, 0.1), n) #+ complex(0,1) * rand(Normal(0, 0.1), n)
    b = complex(1,0) * rand(Normal(0, 0.1), m) #+ complex(0,1) * rand(Normal(0, 0.1), m)
    w = complex(1,0) * rand(Normal(0, 0.1), n, m) #+ complex(0,1) * rand(Normal(0, 0.1), n, m)
    theta = b + w.' * state
    coshprod = cosh(theta[1])
    for j=2:m
        coshprod *= cosh(theta[j])
    end
    theta_upd = copy(theta)
    coshprod_upd = 1.0 #complex(1.0, 0.0)
    w_tot = [a; b; reshape(w, n * m)]
    energy = zeros(Float64, mc_trials) #zeros(Complex{Float64}, mc_trials)
    netparameters = ParamStruct(state, a, b, w, theta, coshprod, theta_upd, coshprod_upd, w_tot, energy)
    return netparameters
end

function init_derivs(VarDervStruct)     # Initiates the variational derivates and passes to the struct
    o_a = zeros(n, mc_trials)
    o_b = zeros(m, mc_trials)
    o_w = zeros(n, m, mc_trials)
    derivs = VarDervStruct(o_a, o_b, o_w)
    return derivs
end

function update_theta(theta_upd::Array{Float64, 1}, w::Array{Float64, 2}, state::Array{Int,1}, flips::Array{Int,1})    # Generates the updated theta given spin flips
    for j=1:m
        for i in flips
             theta_upd[j] -= 2 * w[i,j] * state[i]
        end
    end
end

function psi_mh(NetParams::NetworkParameters, flips::Array{Int,1}; revert=0) # Returns the psi*/psi ratio for energy and MH purposes
    update_theta(NetParams.theta_upd, NetParams.w, NetParams.state, flips)
    logbias = -2 * sum(NetParams.a[flips] .* NetParams.state[flips])
    NetParams.coshprod_upd = cosh(NetParams.theta_upd[1])
    for j=2:m
        NetParams.coshprod_upd *= cosh(NetParams.theta_upd[j])
    end
    if revert==1
        NetParams.theta_upd = copy(NetParams.theta)    # Used when energy is calculated
    end

    return exp(logbias) * NetParams.coshprod_upd / NetParams.coshprod
end

function flip(NetParams::NetworkParameters)
    flips = sample(1:n, nflips, replace=false)   # Sample the flip indices
    if abs2(psi_mh(NetParams, flips)) > rand()   # MH criterion
        NetParams.theta = copy(NetParams.theta_upd)   # Copy updated theta to old theta
        NetParams.coshprod = copy(NetParams.coshprod_upd)
        NetParams.state[flips] *= -1             # Flip the indices
    else
        NetParams.theta_upd = copy(NetParams.theta)
    end
end


function find_energy(NetParams, HamiltWeights, t)     # Function for saving the energy of the network given the current parameters and the trial number

    eloc = 0.0
    for i=1:n
        eloc += HamiltWeights.w_x[i] * psi_mh(NetParams, [i], revert=1)
        eloc += HamiltWeights.w_z[i] * NetParams.state[i]
        for j=i+1:n
            eloc += (HamiltWeights.w_xx[i, j] - HamiltWeights.w_yy[i, j] * NetParams.state[i] * NetParams.state[j]) * psi_mh(NetParams, [i, j], revert=1)
            eloc += HamiltWeights.w_zz[i, j] * NetParams.state[i] * NetParams.state[j]
        end
    end

    NetParams.energy[t] = eloc  # Saves the energy
end


function derivs(VarDervs::VariationalDerivs, NetParams::NetworkParameters, t::Int)
    VarDervs.o_a[:, t] = NetParams.state
    VarDervs.o_b[:, t] = tanh.(NetParams.theta)
    for i=1:n
        for j=1:m
            VarDervs.o_w[i, j, t] = NetParams.state[i] * tanh(NetParams.theta[j])
        end
    end
end

function thermalize(NetParams::NetworkParameters)
    for i=1:thermfactor * mc_trials
        for j=1:mc_steps
            flip(NetParams)
        end
    end
end

function mc_sampling(NetParams::NetworkParameters, VarDervs::VariationalDerivs, HamiltWeights::Weights)
    for i=1:mc_trials
        for j=1:mc_steps
            flip(NetParams)
        end
        derivs(VarDervs, NetParams, i)
        find_energy(NetParams, HamiltWeights, i)
    end
end
