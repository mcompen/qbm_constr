#=
initializer_r:
- Julia version: 0.6.2
- Author: Manu Compen
- Date: 2018-10-01
=#
struct Weights
    w_x::Array{Float64, 1}
    w_y::Array{Float64, 1}
    w_z::Array{Float64, 1}
    w_xx::Array{Float64, 2}
    w_yy::Array{Float64, 2}
    w_zz::Array{Float64, 2}
end

struct Statistics
    σ_x::Array{Float64, 1}
    σ_y::Array{Float64, 1}
    σ_z::Array{Float64, 1}
    σ_xx::Array{Float64, 2}
    σ_yy::Array{Float64, 2}
    σ_zz::Array{Float64, 2}
end

struct Operators
    σ_x::Array{SparseMatrixCSC{Int, Int}, 1}
    σ_y::Array{SparseMatrixCSC{Complex{Int}, Int}, 1}
    σ_z::Array{SparseMatrixCSC{Int, Int}, 1}
    σ_xx::Array{SparseMatrixCSC{Int, Int,}, 2}
    σ_yy::Array{SparseMatrixCSC{Complex{Int}, Int}, 2}
    σ_zz::Array{SparseMatrixCSC{Int, Int}, 2}
end

mutable struct NetworkParameters                    # Struct for typing of all network parameters
    state::Array{Int,1}                     # State of visible neurons
    a::Array{Float64, 1} #Array{Complex{Float64}, 1}           # Bias on visible neurons
    b::Array{Float64, 1} #Array{Complex{Float64}, 1}           # Hidden neurons
    w::Array{Float64, 2} #Array{Complex{Float64}, 2}           # Connection matrix
    theta::Array{Float64, 1} #Array{Complex{Float64}, 1}       # Look-up table
    coshprod::Float64 #Complex{Float64}
    theta_upd::Array{Float64, 1} #Array{Complex{Float64}, 1}   # Updated look-up table for a MH step
    coshprod_upd::Float64 #Complex{Float64}
    w_tot::Array{Float64, 1} #Array{Complex{Float64},1}        # Vector containing all network weights
    energy::Array{Float64, 1} #Array{Complex{Float64},1}
end

mutable struct VariationalDerivs                     # Struct for variational derivatives
    o_a::Array{Int, 2}                       # Var. deriv. wrt a
    o_b::Array{Float64, 2}          # Var. deriv. wrt b
    o_w::Array{Float64, 3}          # Var. deriv. wrt w
end

struct OptimizerParameters                  # Sets the types of the optimization variables
    s::Array{Float64, 2}
    o_tot::Array{Float64, 2}       # Variational derivatives over MC trials
    mo_tot::Array{Float64, 1}      # Mean of o_tot over all trials
    co_tot::Array{Float64, 2}      # Complex conjugate of o_tot
    mco_tot::Array{Float64, 1}     # Mean of complex conjugate of o_tot
    f::Array{Float64, 1}
end