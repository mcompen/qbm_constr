#=
observables_expl:
- Julia version: 0.6.2
- Author: Manu Compen
- Date: 2018-05-16
=#

function init_op(OpStruct)
    σ_x = Array{SparseMatrixCSC{Int, Int}, 1}(n)
    σ_y = Array{SparseMatrixCSC{Complex{Int}, Int}, 1}(n)
    σ_z = Array{SparseMatrixCSC{Int, Int}, 1}(n)
    σ_xx = Array{SparseMatrixCSC{Int, Int,}, 2}(n,n)
    σ_yy = Array{SparseMatrixCSC{Complex{Int}, Int}, 2}(n,n)
    σ_zz = Array{SparseMatrixCSC{Int, Int}, 2}(n,n)

    for i=1:n
        σ_x[i] = spin_operator('x', i)
        σ_y[i] = spin_operator('y', i)
        σ_z[i] = spin_operator('z', i)
    end

    for i=1:n
        for j=1:n
            σ_xx[i,j] = σ_x[i] * σ_x[j]
            σ_yy[i,j] = σ_y[i] * σ_y[j]
            σ_zz[i,j] = σ_z[i] * σ_z[j]
        end
    end

    data_op = OpStruct(σ_x, σ_y, σ_z, σ_xx, σ_yy, σ_zz)
    return data_op
end


function make_hamilt(Weights, Op)   # Nearest neighbors
        hamilt = spzeros(Complex{Float64}, 2^n, 2^n)
        for i=1:n
            hamilt += Op.σ_x[i] * Weights.w_x[i] + Op.σ_y[i] * Weights.w_y[i] + Op.σ_z[i] * Weights.w_z[i]
        end
        for i=1:n-1
            for j=i+1:n
            hamilt += Op.σ_xx[i, j] * Weights.w_xx[i, j] + Op.σ_yy[i, j] * Weights.w_yy[i, j] + Op.σ_zz[i, j] * Weights.w_zz[i, j]
            end
        end
#         return Hermitian(hamilt)
        return hamilt
end

function spin_operator(k::Char, i::Int)
    const id = speye(Int, 2)
    const pauli_x = sparse([0 1; 1 0])
    const pauli_y = sparse([0 -1im; 1im 0])
    const pauli_z = sparse([1 0; 0 -1])

    if i!=1
        sigma = speye(Int, 2)
        for j=1:i-2
            sigma = kron(sigma, id)
        end

        if k=='x'
            sigma = kron(sigma, pauli_x)
        elseif k=='y'
            sigma = kron(sigma, pauli_y)
        else
            sigma = kron(sigma, pauli_z)
        end
    else
        if k=='x'
            sigma = pauli_x
        elseif k=='y'
            sigma = pauli_y
        else
            sigma = pauli_z
        end
    end

    for j=1:n-i
        sigma = kron(sigma, id)
    end
    return sigma
end

