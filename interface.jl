#=
interface:
- Julia version: 0.6.2
- Author: Manu Compen
- Date: 2018-10-18
=#

# SEED
# srand(9999)

# QBM params
const n = 6                     # Number of visible neurons
const pbc = true                # Periodic boundary conditions
const method = "carleo"         # Method of generating model statistics and density matrix. extreme_iter (iterative diagonalization) / extreme (non-iterative diagonalization) / exact (form density matrix from exp(-H)) / carleo
const nev = 1                   # Dimension of density matrices (not used for carleo method)
const model_init = "constr_rand"# QBM model weight initialisation
const data_init = "constr_test" # Name of data weights type
const momentum = 0.5            # Momentum
const iter_max = 3000           # Maximum total iterations of QBM
const save_figures = true       # Save correlation and llhood plots
const qbm_lr_init = 0.1         # Initial QBM learning rate

# Carleo params
const repetitions = 500   # Number of mean energy calculations
const α = 4                # No. hidden divided by No. visible
const γ = 0.05             # Learning parameter
const thermfactor = 0.2    # Thermalisation steps = thermfactor * mc_trials
const mc_trials = 200      # Number of var derivatives per repetition
const mc_steps = n         # Number of MH steps per var derivative
const nflips = 1           # Number of flips to be performed per MH step
