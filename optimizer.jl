#=
optimizer:
- Julia version: 0.6.2
- Author: Manu Compen
- Date: 2018-05-09
=#

function init_optim(OptimParamStruct)       # Loads an Optimizer constructor and initiates
    s = zeros(dim, dim)
    o_tot = zeros(dim, mc_trials)
    mo_tot = zeros(dim)
    co_tot = zeros(dim, mc_trials)
    mco_tot = zeros(dim)
    f = zeros(dim)
    optimparameters = OptimParamStruct(s, o_tot, mo_tot, co_tot, mco_tot, f)
    return optimparameters
end

function optimize(NetParams, VarDervs, Opt, λ)
    Opt.o_tot .= vcat(VarDervs.o_a + 0.0im, VarDervs.o_b, reshape(VarDervs.o_w, n * m, mc_trials))
    Opt.mo_tot .= vec(mean(Opt.o_tot, 2))                # Mean over MC trials
    Opt.co_tot .= conj(Opt.o_tot)                        # Complex conjugate
    Opt.mco_tot .= vec(mean(Opt.co_tot, 2))              # Mean of o*
    Opt.f .= vec(mean(NetParams.energy.' .* Opt.co_tot, 2) .- mean(NetParams.energy) * Opt.mco_tot)

    for i=1:dim
        for j=i:dim
            Opt.s[i,j] = mean(Opt.co_tot[i, :] .* Opt.o_tot[j, :]) - Opt.mco_tot[i] * Opt.mo_tot[j]
            Opt.s[j,i] = conj(Opt.s[i, j])
        end
    end
    NetParams.w_tot .-= γ * inv(λ * eye(dim) + Opt.s) * Opt.f
    NetParams.a .= NetParams.w_tot[1:n]
    NetParams.b .= NetParams.w_tot[n + 1: n + m]
    NetParams.w[:,:] = NetParams.w_tot[n + m + 1: end]
    NetParams.theta .= NetParams.b + NetParams.w.' * NetParams.state
    NetParams.theta_upd .= NetParams.theta
end

