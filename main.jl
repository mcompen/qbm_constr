#=
main::
- Julia version: 0.6.2
- Author: Manu Compen
- Date: 2018-05-04
=#

# PACKAGES
using StatsBase          # Used for sampling flip indices without replacement
# using Plots
using Distributions
using Plots
clibrary(:cmocean)
import GR                  # https://github.com/JuliaPlots/Plots.jl/issues/1047
ENV["GKSwstype"] = "100"   # https://github.com/JuliaPlots/Plots.jl/issues/1076

# Parameters
include("interface.jl")    # Contains all parameters

# QBM files
include("initializer.jl")  # Contains all relevant variable-structs
include("weights.jl")      # Contains hamiltonian weights w_i^k and w_ij^k
include("plotting.jl")     # Some functions for plotting
include("hamilt.jl")       # Generates sparse hamiltonians with spin-operators
include("statistics.jl")   # Contains several methods for statistics generation
include("findstate.jl")    # Contains methods for finding extreme eigenvectors / values

# Carleo files
include("optimizer.jl")    # Inversion of Sorella matrix and updating of network parameters
include("network.jl")      # Sweeping / energy calculation
include("main_carleo.jl")  # Main Carleo/Troyer loop

const Op = init_op(Operators) # Spin-operators for dimension n
const dataweights = normalizer(init_weights(Weights, data_init)) # Normalized dataweights

println("Learning $n spin $data_init system with $model_init model initialization weights and \n pbc=$pbc \n method=$method \n momentum=$momentum \n max_qbm_iter=$iter_max")
if method == "carleo"
    println(" α=$α\n carleo iterations=$repetitions \n samples=$mc_trials")
end
println(" Saving figures=$save_figures")

function main()
    println("Calculating data statistics")
    NaNflag = false # Checks if the carleo algorithm returns infs or nans
    datahamilt, ψ_η, eigvalsdata = extreme_eig(dataweights, evs=1)
    datastat = extreme_statistics(ψ_η, eigvalsdata) # pure ground state data statistics
    println("Starting QBM")

    modelweights = normalizer(init_weights(Weights, model_init))
    saveweights = deepcopy(modelweights) # Saves the model hamiltonian weights with largest llhood

    i = 0
    rejects = 0         # Rejected consecutive QBM iterations
    llhood_old = -999
    llhood = -999
    dL = 999            # Change in L from iteration i to i+1
    llhood_iter = Array{Float64, 1}(0) # Array to store llhoods every QBM iteration
    qbm_lr = qbm_lr_init
    dw_x = 0.0 # Change in w_i^x
    dw_y = 0.0
    dw_z = 0.0
    dw_xx = 0.0
    dw_yy = 0.0
    dw_zz = 0.0

    if method == "extreme_iter"
        modelhamilt, ψ_ρ, eigvalsmodel = extreme_eigs(modelweights, evs=nev)
        modelstat = extreme_statistics(ψ_ρ, eigvalsmodel)
    elseif method == "extreme"
        modelhamilt, ψ_ρ, eigvalsmodel = extreme_eig(modelweights, evs=nev)
        modelstat = extreme_statistics(ψ_ρ, eigvalsmodel)
    elseif method == "exact"
        modelhamilt, ρ = exact_dm(modelweights)
        modelstat = exact_statistics(ρ)
    elseif method == "carleo"
        modelhamilt = make_hamilt(modelweights, Op)
        modelstat, ψ_ρ, eigvalsmodel, NaNflag = main_carleo(modelweights)
    end

    while i<iter_max && llhood < -1e-8 && dL > 1e-10 && rejects < 5
        if i>1 || NaNflag
            if method == "extreme_iter" || method == "extreme"
                modelstat = extreme_statistics(ψ_ρ, eigvalsmodel)
            elseif method == "exact"
                modelstat = exact_statistics(ρ)
            elseif method == "carleo" # Always recalculate statistics in case of carleo statistics
                modelstat, ψ_ρ, eigvalsmodel, NaNflag = main_carleo(modelweights)
            end
        end

        if ~NaNflag
        dw_x = qbm_lr * (-datastat.σ_x + modelstat.σ_x + momentum * dw_x) # QBM learning step
        dw_y = qbm_lr * (-datastat.σ_y + modelstat.σ_y + momentum * dw_y)
        dw_z = qbm_lr * (-datastat.σ_z + modelstat.σ_z + momentum * dw_z)

        dw_xx = qbm_lr * (-datastat.σ_xx + modelstat.σ_xx + momentum * dw_xx)
        dw_yy = qbm_lr * (-datastat.σ_yy + modelstat.σ_yy + momentum * dw_yy)
        dw_zz = qbm_lr * (-datastat.σ_zz + modelstat.σ_zz + momentum * dw_zz)

        modelweights.w_x .+= dw_x
        modelweights.w_y .+= dw_y
        modelweights.w_z .+= dw_z
        modelweights.w_xx .+= dw_xx
        modelweights.w_yy .+= dw_yy
        modelweights.w_zz .+= dw_zz

        modelweights = normalizer(modelweights)
        modelweights = w_constrainer(modelweights)

        if method == "extreme_iter"
            modelhamilt, ψ_ρ, eigvalsmodel = extreme_eigs(modelweights, evs=nev, v0=ψ_ρ[:, 1])
            z = sum(exp.(-eigvalsmodel))
            llhood = real(trace(-modelhamilt * ψ_η * ψ_η')) - log(z)
        elseif method == "extreme"
            modelhamilt, ψ_ρ, eigvalsmodel = extreme_eig(modelweights, evs=nev)
            z = real.(sum(exp.(-eigvalsmodel)))
            llhood = real(trace(-modelhamilt * ψ_η * ψ_η')) - log(z)
        elseif method == "exact"
            modelhamilt, ρ, z = exact_dm(modelweights)
            llhood = real(trace(-modelhamilt * ψ_η * ψ_η')) - log(z)
        elseif method == "carleo"
            modelhamilt = make_hamilt(modelweights, Op)
            eigvalsmodel = eigmin(full(modelhamilt))
            llhood = real(trace(-modelhamilt * ψ_η * ψ_η')) + eigvalsmodel
        end

        end

        if llhood > llhood_old
            dL = abs(llhood_old - llhood)
            llhood_old = llhood
            qbm_lr = min(qbm_lr * 1.01, 2)
            rejects = 0
            saveweights = deepcopy(modelweights)
        else
            if isnan(llhood)
                println("llhood NaN")
            end
            modelweights = deepcopy(saveweights)
            rejects += 1
            dw_x = 0
            dw_y = 0
            dw_z = 0
            dw_xx = 0
            dw_yy = 0
            dw_zz = 0
            qbm_lr = max(0.0001, qbm_lr * 0.5)
            llhood = llhood_old
        end

        println("L=$llhood")
        if i%100 == 0
            println("i = $i")
        end

        append!(llhood_iter, llhood)
        i += 1
    end

    if method == "extreme_iter"
        ψ_ρ = ψ_ρ[:, 1]
    elseif method == "extreme"
        ψ_ρ = ψ_ρ[:, 1]
    elseif method == "exact"
        eigv = eigfact(full(modelhamilt))
        ψ_ρ = eigv[:vectors][:, 1]
    elseif method == "carleo"
        nothing
    end

#     DEPHASING
#     ψ_ρ_norms = norm.(ψ_ρ)
#     ψ_ρ_phases = angle.(ψ_ρ)
#     phase = ψ_ρ_phases[indmax(ψ_ρ_norms)]
#     ψ_ρ = ψ_ρ_norms .* exp.((ψ_ρ_phases - phase)*im)
#     ψ_ρ = real.(ψ_ρ) / norm(real.(ψ_ρ))

    println("ψ_ρ = $ψ_ρ \n")
    println("ψ_η = $ψ_η \n")
    println("MODELWEIGHTS = $modelweights \n")
    println("DATAWEIGHTS = $dataweights \n")
    min_norm = min(norm(ψ_η - ψ_ρ), norm(ψ_η + ψ_ρ)) / norm(ψ_η) # Relative error up to a global sign
    println("Δψ = $min_norm IN $i ITERATIONS")
    println("L = $llhood")

    if save_figures == true
        println("Plotting data statistics and weights")
        plot_psis(ψ_η, ψ_ρ)
        plot_statistics(datastat, "data")
        plot_weights(dataweights, "data")
        println("Plotting model statistics and weights")
        plot_statistics(modelstat, "model")
        plot_weights(modelweights, "model")
        plot_likelihood(llhood_iter)
    end
end

function w_constrainer(weights::Weights) ## Primitive constrainment
#     weights.w_x .= 0
    weights.w_y .= 0
    weights.w_z .= 0
    weights.w_x[weights.w_x .> 0] .= 0
#     weights.w_yy[weights.w_xx .> 0] .= 0
    weights.w_xx[weights.w_xx .> 0] .= 0

    for i=1:n
        for j=1:n
            if weights.w_xx[i, j] > -abs(weights.w_yy[i, j])
                if weights.w_yy[i, j] > 0
                    weights.w_xx[i, j] = weights.w_xx[i, j] - (weights.w_yy[i,j] + weights.w_xx[i, j]) / 2
                    weights.w_yy[i, j] = -weights.w_xx[i, j]
                elseif weights.w_yy[i, j] < 0
                    weights.w_xx[i, j] = weights.w_xx[i, j] + (weights.w_yy[i, j] - weights.w_xx[i, j]) / 2
                    weights.w_yy[i, j] = weights.w_xx[i, j]
                end
            end
        end
    end
    return weights
end

# TIMING
for i=1:1
    @time main()
end

# PROFILING
# using ProfileView
# @profile main()
# Profile.print()

