#=
findstate:
- Julia version: 0.6.2
- Author: Manu Compen
- Date: 2018-06-18
=#

function extreme_eig(weights::Weights; evs=1) # hamilt.jl
    hamilt = make_hamilt(weights, Op)
    eigv = eigfact(full(hamilt))
    evs = min(length(eigv[:values]), evs)
    if evs > 1
        ψ = zeros(Complex{Float64}, 2^n, evs)
#         eigvals = eigv[:values][end-evs: end]
        eigvals = eigv[:values][1:evs]
        for k=1:evs
#             ψ[:, k] = eigv[:vectors][:, end - evs + k]
            ψ[:, k] = eigv[:vectors][:, k]
        end
    else
#         eigvals, eigind = findmin(eigv[:values])
#         ψ = eigv[:vectors][:, eigind]
        eigvals = eigv[:values][1]
        ψ = eigv[:vectors][:, 1]

        if method == "carleo"
            if any(abs.(imag(ψ)) .> 1e-12)
                println("WARNING: ψ imaginary in extreme_eig")
            end

            ψ = real.(ψ)

            if all(ψ .< 1e-12)
                ψ *= -1
            elseif any(ψ .< -1e-3) && any(ψ .> 1e3)
                println("ERROR: ψ in extreme_eig not purely negative/positive. Wrong constraints?")
            end
        end
    end
    return hamilt, ψ, eigvals
end

function extreme_eigs(weights::Weights; evs=1, v0=0) # hamilt.jl
    hamilt = make_hamilt(weights, Op)
    if v0==0
        eigvals, ψ  = eigs(-hamilt, nev=evs, which=:LR)
        eigvals = -real.(eigvals) # algorithm produces small ~10^(-15) complex parts
    else
        eigvals, ψ  = eigs(-hamilt, nev=evs, v0=v0, which=:LR)
        eigvals = -real.(eigvals)
    end

    if nev == 1
        ψ = vec(ψ)
    end
    return hamilt, ψ, eigvals
end

function exact_dm(weights::Weights)
    hamilt = make_hamilt(weights, Op)
    exp_h = expm(-full(hamilt))
    z = real(trace(exp_h))
    ρ = exp_h / z
    ρ = 0.5 * (ρ + ρ')
    return hamilt, ρ, z
end

