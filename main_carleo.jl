#=
main::
- Julia version: 0.6.2
- Author: Manu Compen
- Date: 2018-07-06
=#

function main_carleo(WeightsQBM)
    regulator(p) = 0.01 #max(10.0 ^ (-4), 100 * 0.9 ^ p)
    netparams = init_params(NetworkParameters)
    dervs = init_derivs(VariationalDerivs)
    optparams = init_optim(OptimizerParameters)
    thermalize(netparams)
    mean_e = 999
    NaNflag = false

    for i=1:repetitions
        mc_sampling(netparams, dervs, WeightsQBM)        # Run the MC sampling
        mean_e = mean(netparams.energy)      # Determine the mean energy over the MC samples
        if isnan(mean_e)
            println("NaNflag activated")
            NaNflag = true
            break
        end
        optimize(netparams, dervs, optparams, regulator(i))    # Optimize the parameters
    end
    println("energy = $mean_e")
    stat, ψ_ρ = sampled_statistics(netparams)
    return stat, ψ_ρ, mean_e, NaNflag
end

