#=
weights:
- Julia version: 0.6.2
- Author: Manu Compen
- Date: 2018-05-28
=#

# Generates single site and pair interactions hamiltonian

struct Weights
    w_x::Array{Float64, 1}
    w_y::Array{Float64, 1}
    w_z::Array{Float64, 1}
    w_xx::Array{Float64, 2}
    w_yy::Array{Float64, 2}
    w_zz::Array{Float64, 2}

    function Base.:-(a::Weights)
        w_x = -a.w_x
        w_y = -a.w_y
        w_z = -a.w_z
        w_xx = -a.w_xx
        w_yy = -a.w_yy
        w_zz = -a.w_zz
        Weights(w_x, w_y, w_z, w_xx, w_yy, w_zz)
    end
end

function normalizer(a::Weights)
    max_w = maximum(abs, hcat(a.w_x, a.w_y, a.w_y, a.w_xx, a.w_yy, a.w_zz))
    w_x = a.w_x / max_w
    w_y = a.w_y / max_w
    w_z = a.w_z / max_w
    w_xx = a.w_xx / max_w
    w_yy = a.w_yy / max_w
    w_zz = a.w_zz / max_w
    Weights(w_x, w_y, w_z, w_xx, w_yy, w_zz)
end

function init_weights(WeightsStruct, model)

    if model == "afh"
        w_x = zeros(n); w_y = zeros(n); w_z = zeros(n)
        w_xx = zeros(n,n) ; w_yy = zeros(n,n); w_zz = zeros(n,n)
        for i=1:n - 1
            j=i + 1
            w_xx[i, j] = 1; w_yy[i, j] = 1; w_zz[i, j] = 1
        end
        w_xx[1, n] = 1; w_yy[1, n] = 1; w_zz[1, n] = 1
        w_xx += w_xx'; w_yy += w_yy'; w_zz += w_zz'

    elseif model == "afh_conn"
        w_x = zeros(n); w_y = zeros(n); w_z = zeros(n)
        w_xx = -ones(n,n) + diagm(ones(n)) ; w_yy = -ones(n,n) + diagm(ones(n)) ; w_zz = -ones(n,n) + diagm(ones(n))

    elseif model == "afh_ext_y"
        w_x = zeros(n); w_y = -ones(n); w_z = zeros(n)
        w_xx = zeros(n,n) ; w_yy = zeros(n,n); w_zz = zeros(n,n)
        for i=1:n - 1
            j=i + 1
            w_xx[i, j] = -1; w_yy[i, j] = -1; w_zz[i, j] = -1
        end
        w_xx[1, n] = -1; w_yy[1, n] = -1; w_zz[1, n] = -1
        w_xx += w_xx'; w_yy += w_yy'; w_zz += w_zz'

    elseif model == "afh_xyz"
        w_x = zeros(n); w_y = zeros(n); w_z = zeros(n)
        w_xx = zeros(n,n) ; w_yy = zeros(n,n); w_zz = zeros(n,n)
        for i=1:n - 1
            j=i + 1
            w_xx[i, j] = -0.2; w_yy[i, j] = -0.6; w_zz[i, j] = -0.4
        end
        w_xx[1, n] = -0.2; w_yy[1, n] = -0.6; w_zz[1, n] = -0.4
        w_xx += w_xx'; w_yy += w_yy'; w_zz += w_zz'

    elseif model == "afh_xyz_ext"
        w_x = rand(Normal(0, 1/sqrt(n)), n); w_y = rand(Normal(0, 1/sqrt(n)), n); w_z = rand(Normal(0, 1/sqrt(n)), n)
        w_xx = zeros(n,n) ; w_yy = zeros(n,n); w_zz = zeros(n,n)
        for i=1:n - 1
            j=i + 1
            w_xx[i, j] = -0.2; w_yy[i, j] = -0.6; w_zz[i, j] = -0.4
        end
        w_xx[1, n] = -0.2; w_yy[1, n] = -0.6; w_zz[1, n] = -0.4
        w_xx += w_xx'; w_yy += w_yy'; w_zz += w_zz'

    elseif model == "afh_noisy"
        w_x = 0.01 * rand(n); w_y = 0.01 * rand(n); w_z = 0.01 * rand(n)
        w_xx = 0.01 * rand(n,n) ; w_yy = 0.01 * rand(n,n); w_zz = 0.01 * rand(n,n)
        for i=1:n - 1
            j=i + 1
            w_xx[i, j] -= 1; w_yy[i, j] -= 1; w_zz[i, j] -= 1
        end
        w_xx[1, n] -= 1; w_yy[1, n] -= 1; w_zz[1, n] -= 1
        w_xx += w_xx'; w_yy += w_yy'; w_zz += w_zz'

    elseif model == "random_normal"
        w_x = rand(Normal(0, 1e-5), n); w_y = rand(Normal(0, 1e-5), n); w_z = rand(Normal(0, 1e-5), n)
        w_xx = rand(Normal(0, 1e-5), n, n); w_yy = rand(Normal(0, 1e-5), n, n); w_zz = rand(Normal(0, 1e-5), n, n)

    elseif model == "random_normal_ext0_symm"
        w_x = zeros(n); w_y = zeros(n); w_z = zeros(n)
        w_xx = w_yy = w_zz = rand(Normal(0, 1e-5), n, n)

    elseif model == "random"
        w_x = rand(n); w_y = rand(n); w_z = rand(n)
        w_xx = rand(n, n); w_yy = rand(n, n); w_zz = rand(n, n)

    elseif model == "random_ext0"
        w_x = zeros(n); w_y = zeros(n); w_z = zeros(n)
        w_xx = rand(n, n); w_yy = rand(n, n); w_zz = rand(n, n)

    elseif model == "random_y0"
        w_x = rand(n); w_y = zeros(n); w_z = rand(n)
        w_xx = rand(n, n); w_yy = rand(n, n); w_zz = rand(n, n)

    elseif model == "ones"
        w_x = ones(n); w_y = ones(n); w_z = ones(n)
        w_xx = ones(n, n); w_yy = ones(n, n); w_zz = ones(n, n)

    elseif model == "constr_test"
        w_x = zeros(n); w_y = zeros(n); w_z = zeros(n)
        w_xx = zeros(n,n) ; w_yy = zeros(n,n); w_zz = zeros(n,n)
        for i=1:n - 1
            j=i + 1
            w_xx[i, j] = -0.3; w_yy[i, j] = 0.2; w_zz[i, j] = 0.1
        end
        w_xx[1, n] = -0.3; w_yy[1, n] = 0.2; w_zz[1, n] = 0.1
        w_xx += w_xx'; w_yy += w_yy'; w_zz += w_zz'

    elseif model == "constr_test2"
        w_x = -0.1 * ones(n); w_y = zeros(n); w_z = zeros(n)
        w_xx = zeros(n,n) ; w_yy = zeros(n,n); w_zz = zeros(n,n)
        for i=1:n - 1
            j=i + 1
            w_xx[i, j] = -0.9; w_yy[i, j] = 0.1; w_zz[i, j] = 0.3
        end
        w_xx[1, n] = -0.9; w_yy[1, n] = 0.1; w_zz[1, n] = 0.3
        w_xx += w_xx'; w_yy += w_yy'; w_zz += w_zz'

    elseif model == "constr_rand"
        w_x = -0.05 * rand(n); w_y = zeros(n); w_z = zeros(n)
        w_xx = zeros(n,n) ; w_yy = zeros(n,n); w_zz = zeros(n,n)
        for i=1:n - 1
            j=i + 1
            w_yy[i, j] = rand(Normal(0, 1e-5)); w_zz[i, j] = rand(Normal(0, 1e-5))
            w_xx[i, j] = rand(Uniform(-abs(w_yy[i, j]) - 1e-1, -abs(w_yy[i, j])))
        end
        w_yy[1, n] = rand(Normal(0, 1e-5)); w_zz[1, n] = rand(Normal(0, 1e-5))
        w_xx[1, n] = rand(Uniform(-abs(w_yy[1, n]) - 1e-1, -abs(w_yy[1, n])))
        w_xx += w_xx'; w_yy += w_yy'; w_zz += w_zz'

    elseif model == "zeros"
        w_x = w_y = w_z = zeros(n);
        w_xx = w_yy = w_zz = zeros(n,n);

    else
        println("No model with that name")
    end

    if ~pbc && n>2
        w_xx[1, n] = w_yy[1, n] = w_zz[1, n] = 0
        w_xx[n, 1] = w_yy[n, 1] = w_zz[n, 1] = 0
    end

    # Symmetrize and remove diagonal
    w_xx = (w_xx + w_xx.') / 2; w_yy = (w_yy + w_yy.') / 2; w_zz = (w_zz + w_zz.') / 2
    w_xx -= diagm(diag(w_xx)); w_yy -= diagm(diag(w_yy)); w_zz -= diagm(diag(w_zz))

    weights = WeightsStruct(w_x, w_y, w_z, w_xx, w_yy, w_zz)
    return weights
end

