#=
statistics:
- Julia version: 0.6.2
- Author: Manu Compen
- Date: 2018-06-18
=#

function init_stat(statStruct)
    σ_x = zeros(Float64, n)
    σ_y = zeros(Float64, n)
    σ_z = zeros(Float64, n)
    σ_xx = zeros(Float64, n, n)
    σ_yy = zeros(Float64, n, n)
    σ_zz = zeros(Float64, n, n)
    stat = statStruct(σ_x, σ_y, σ_z, σ_xx, σ_yy, σ_zz)
    return stat
end

function exact_statistics(ρ)
    stat = init_stat(Statistics)
    for i=1:n
        stat.σ_x[i] = real(trace(Op.σ_x[i] * ρ))
        stat.σ_y[i] = real(trace(Op.σ_y[i] * ρ))
        stat.σ_z[i] = real(trace(Op.σ_z[i] * ρ))
        for j=i+1:n
            stat.σ_xx[i, j] = real(trace(Op.σ_xx[i, j] * ρ))
            stat.σ_yy[i, j] = real(trace(Op.σ_yy[i, j] * ρ))
            stat.σ_zz[i, j] = real(trace(Op.σ_zz[i, j] * ρ))
        end
    end

    stat.σ_xx .+= stat.σ_xx'
    stat.σ_yy .+= stat.σ_yy'
    stat.σ_zz .+= stat.σ_zz'
    return stat
end

function extreme_statistics(ψ, eigvals)
    stat = init_stat(Statistics)
    z = sum(exp.(-eigvals))
    if ~isa(eigvals, Array)
        eigvals = [eigvals]
    end
    for k=1:length(eigvals)
        for i=1:n
            stat.σ_x[i] += exp(-eigvals[k]) * real.(ψ[:, k]' * Op.σ_x[i] * ψ[:, k])
            stat.σ_y[i] += exp(-eigvals[k]) * real.(ψ[:, k]' * Op.σ_y[i] * ψ[:, k])
            stat.σ_z[i] += exp(-eigvals[k]) * real.(ψ[:, k]' * Op.σ_z[i] * ψ[:, k])
            for j=i+1:n
                stat.σ_xx[i, j] += exp(-eigvals[k]) * real.(ψ[:, k]' * Op.σ_xx[i, j] * ψ[:, k])
                stat.σ_yy[i, j] += exp(-eigvals[k]) * real.(ψ[:, k]' * Op.σ_yy[i, j] * ψ[:, k])
                stat.σ_zz[i, j] += exp(-eigvals[k]) * real.(ψ[:, k]' * Op.σ_zz[i, j] * ψ[:, k])
            end
        end
    end

    stat.σ_x ./= z
    stat.σ_y ./= z
    stat.σ_z ./= z
    stat.σ_xx .= (stat.σ_xx + stat.σ_xx') / z
    stat.σ_yy .= (stat.σ_yy + stat.σ_yy') / z
    stat.σ_zz .= (stat.σ_zz + stat.σ_zz') / z

#     stat.σ_xx ./= maximum(abs, stat.σ_xx)
#     stat.σ_yy ./= maximum(abs, stat.σ_yy)
#     stat.σ_zz ./= maximum(abs, stat.σ_zz)
    return stat
end

function sampled_statistics(NetParams)
    function all_states()
        s_all = zeros(Int, n, 2^n)
        for i=1:2^n
            s_all[:, i] = digits(i - 1, 2, n)
        end
        s_all = 2 * s_all - 1
        return s_all
    end

    function ψ_loc(NetParams, s)
        tmp_theta = NetParams.b + NetParams.w.' * s
        cosh_prod = complex(1.0, 0.0)
        for j=1:m
            cosh_prod *= 2 * cosh(tmp_theta[j])
        end
        return exp(NetParams.a.' * s) * cosh_prod
    end

    stat = init_stat(Statistics)
    s_all = all_states()
    ψ_tot = zeros(Complex{Float64}, 2^n)

    for i=1:2^n
        ψ_tot[i] = ψ_loc(NetParams, s_all[:, i])
    end

    # DEPHASING
    ψ_tot_norms = norm.(ψ_tot)
    ψ_tot_phases = angle.(ψ_tot)
    phase = ψ_tot_phases[indmax(ψ_tot_norms)]
    ψ_tot = ψ_tot_norms .* exp.((ψ_tot_phases - phase)*im)

    ψ_tot = real.(ψ_tot)
    ψ_tot = ψ_tot / norm(ψ_tot)
    ρ = ψ_tot * ψ_tot'
    ρ /= real.(trace(ρ))

    for i=1:n
        stat.σ_x[i] = real(trace(ρ * Op.σ_x[i]))
        stat.σ_y[i] = real(trace(ρ * Op.σ_y[i]))
        stat.σ_z[i] = real(trace(ρ * Op.σ_z[i]))
        for j=i+1:n
            stat.σ_xx[i, j] = real(trace(ρ * Op.σ_xx[i, j]))
            stat.σ_yy[i, j] = real(trace(ρ * Op.σ_yy[i, j]))
            stat.σ_zz[i, j] = real(trace(ρ * Op.σ_zz[i, j]))
        end
    end

    stat.σ_xx .+= stat.σ_xx'
    stat.σ_yy .+= stat.σ_yy'
    stat.σ_zz .+= stat.σ_zz'

    return stat, ψ_tot
end